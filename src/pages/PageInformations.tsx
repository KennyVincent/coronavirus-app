import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonText } from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import './Page.css';
import PageTitle from '../components/Design/PageTitle';
import Card from '../components/Design/Card';
import Buttons from '../components/Design/Buttons';

const PageInformations: React.FC<RouteComponentProps<{ name: string; }>> = ({ match }) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Informations about the Covid-19</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <PageTitle text="C-19" /> 
        <Card text="Stay home. Stay save. Save lifes." />
        <Buttons url="/global-overview/" textUrl="Global Overview" />
      </IonContent>
    </IonPage>
  );
};

export default PageInformations;
