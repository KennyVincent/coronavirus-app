import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonText } from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import './Page.css';
import Api from '../components/Api/api';
import PageTitle from '../components/Design/PageTitle';
import Buttons from '../components/Design/Buttons';

const PageGlobalOverview: React.FC<RouteComponentProps<{ name: string; }>> = ({ match }) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Global Overview</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
          <PageTitle text="C-19" subtext="Global Status"/>
          <Api />
          <Buttons url="/local-overview/" textUrl="local overview" />
      </IonContent>
    </IonPage>
  );
};

export default PageGlobalOverview;
