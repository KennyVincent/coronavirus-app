import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonText } from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import './Page.css';
import ApiGeoloc from '../components/Api/api-geoloc';
import PageTitle from '../components/Design/PageTitle';
import Buttons from '../components/Design/Buttons';

const PageLocalOverview: React.FC<RouteComponentProps<{ name: string; }>> = ({ match }) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Local Overview</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
          <PageTitle text="C-19" subtext="Local Status"/>
          <ApiGeoloc/>
          <Buttons url="/global-overview/" textUrl="global overview" />
      </IonContent>
    </IonPage>
  );
};

export default PageLocalOverview;
