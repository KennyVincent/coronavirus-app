import React, {  useState, useEffect } from 'react';
import { IonTitle, IonText, IonContent } from '@ionic/react';
import axios from 'axios';

export default function PageTitle(props){
    const [text, setText] = useState('');
    return (
        <div class="container-title">
            <div class="hr"></div>
            <div>
                <div>
                    <div>
                        <h1>{ props.text }</h1>
                        <p>{ props.subtext }</p>
                    </div>
                </div>
            </div>
        </div>
    );

}
