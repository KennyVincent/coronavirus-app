import React, {  useState, useEffect } from 'react';
import { IonTitle, IonText, IonContent } from '@ionic/react';
import axios from 'axios';

export default function Card(props){
    const [text, setText] = useState('');
    return (
        <div class="container-button">
            <div>
                <div>
                    <a href={props.url}>+</a>
                </div>
                <p>Tap to see the {props.textUrl}.</p>
            </div>
        </div>
    );

}
