import React, {  useState, useEffect } from 'react';
import { IonTitle, IonText, IonContent } from '@ionic/react';
import axios from 'axios';

export default function Card(props){
    const [text, setText] = useState('');
    return (
        <div class="container-card">
            <div>
                <h2>{ props.text }</h2>
            </div>
        </div>
    );

}
