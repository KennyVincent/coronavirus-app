import React, {  useState, useEffect } from 'react';
import { IonTitle, IonText, IonContent } from '@ionic/react';
import axios from 'axios';
export default function ApiGeoloc(props){
    const [data, setData] = useState([]);
    const [type, setType] = useState('');
    // const [isLoading, setIsLoading] = useState(false);
    const [url, setUrl] = useState('http://demogit.tp.crea.pro/covid.php');

    const geoloc = async () => {
        navigator.geolocation.getCurrentPosition(async function(position) {
            let latlon = position.coords.latitude+","+position.coords.longitude;
            // setGeo('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlon+'&key=AIzaSyCZJwauN9_GN5zVqvz01bHBjf8zT-97Bw4');
            let urlGeo = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlon+'&key=AIzaSyCZJwauN9_GN5zVqvz01bHBjf8zT-97Bw4';
            const resultGeoloc = await axios(urlGeo);
            const countrySentence = resultGeoloc.data.plus_code.compound_code;
            const country = countrySentence.split(", ");
            const pays = country[1];
            console.log(pays);
            const result = await axios(url);
            let data = result.data.PaysData;
            console.log(data);
            let arrayResult = [];
            let today = new Date();
            let dd = String(today.getDate()).padStart(2, '0');
            let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            let yyyy = today.getFullYear();
            today = yyyy + '-' + mm + '-' + dd + 'T00:00:00';
            data.forEach(item => {
            if(item.Pays === pays && item.Date === today)
                arrayResult.push(item);
            });
            setData(arrayResult);
        });
    }

    useEffect(() => {
        geoloc();
    }, [url]);

    return (
        <div class="container-api">
        <div>
            <h3>Latest informations :</h3>
        </div>
        <div class="container-local-data">
            { data.map(item => (
            <div>
                <h3 key={item.Pays}>{item.Pays}</h3>
                <p>Infections : <span>{item.Infection}</span></p>
                <p>Healings : <span>{item.Guerisons}</span></p>
                <p>Death : <span>{item.Deces}</span></p>
            </div>
            )) }
            </div>
        </div>
    );

}
