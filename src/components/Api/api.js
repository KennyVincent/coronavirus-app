import React, {  useState, useEffect } from 'react';
import { IonTitle, IonText, IonContent } from '@ionic/react';
import axios from 'axios';
export default function Api(props){
    const [data, setData] = useState([]);
    const [dataText, setDataText] = useState('');
    const [type, setType] = useState('');
    // const [isLoading, setIsLoading] = useState(false);
    const [url, setUrl] = useState('http://demogit.tp.crea.pro/covid.php');

  const fetchData = async () => {
    // setIsLoading(true);
    const result = await axios(url);
    setDataText(result.data.Source);
    let data = result.data.PaysData;
    let arrayResult = [];
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd + 'T00:00:00';
    data.forEach(item => {
      if(item.Date === today)
        arrayResult.push(item);
    });
    setData(arrayResult);

    // data = Array.from(data);
  };
  useEffect(() => {
    fetchData();
  }, [url]);

  return (
    <div class="container-api">
      <div>
        <h3>Latest informations :</h3>
      </div>
      <div class="container-global-data">
        { data.map(item => (
          <div>
            <h3 key={item.Pays}>{item.Pays}</h3>
            <p>Infections : <span>{item.Infection}</span></p>
            <p>Healings : <span>{item.Guerisons}</span></p>
            <p>Death : <span>{item.Deces}</span></p>
          </div>
        )) }
        </div>
    </div>
  );

}
