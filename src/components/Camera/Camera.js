import React, {  useState, useEffect } from 'react';
export default function Camera(){
    function initCamera(){
        let video = document.getElementById('video');
        
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
        video.srcObject = stream;
        video.play();
        });
    }
    useEffect(() => {
        initCamera();
      });
    

    return (
        <video id="video" width="640" height="480" autoPlay></video>
    );
}